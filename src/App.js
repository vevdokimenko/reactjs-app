import logo from "./logo.svg";
import "./App.css";
import { Route, Routes } from "react-router-dom";
import { Home, NotFoundPage, Page } from "./pages/Page";
import Navbar from "./pages/Navbar";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Navbar />
        <Routes>
          <Route path="/" index element={<Home />} />
          <Route
            path="/page1"
            element={
              <Page
                title="Image WEBP"
                img={{ src: "img/1.sm.webp", title: "image1" }}
                text="Here is image *.webp"
              />
            }
          />
          <Route
            path="/page2"
            element={
              <Page
                title="Image SVGZ"
                img={{ src: "img/9656938.fig.003.svgz", title: "svgz" }}
                text="Here is image *.svgz"
              />
            }
          />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
