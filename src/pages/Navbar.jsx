import React from "react";
import { Link } from "react-router-dom";
import s from "./Navbar.module.scss";

export default function Navbar(props) {
  return (
    <nav className={s.container}>
      <Link to="/">Home</Link>
      <Link to="page1">Page1</Link>
      <Link to="page2">Page2</Link>
    </nav>
  );
}
