import React from "react";
import { Link } from "react-router-dom";
import s from "./Pages.module.scss";

export function Home(props) {
  return (
    <div>
      <h1>Welcome</h1>
    </div>
  );
}

export function Page(props) {
  const item = props.img ? props.img : "";

  return (
    <div className={s.container}>
      <div className={s.img}>
        <img alt={item.title} src={item.src} />
      </div>
      <div className={s.description}>
        <h3>{props.title}</h3>
        <p>{props.text}</p>
      </div>
    </div>
  );
}

export function NotFoundPage(props) {
  return (
    <div>
      <h1>404</h1>
      <p>
        Page not found. Go <Link to="/">home</Link>
      </p>
    </div>
  );
}
